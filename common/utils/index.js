// export default baseUrl
// const baseUrl = 'https://steelwmstest.cndmaterial.com/framework' //钢铁测试环境
// const baseUrl = '/api'
// const baseUrl = 'https://steelwms.cndmaterial.com/framework' //钢铁正试环境
// #ifdef  H5
const baseUrl = '/api'
// #endif
// #ifdef  MP-WEIXIN
const baseUrl = 'https://steelwmstest.cndmaterial.com/framework'
// #endif
// #ifdef  APP-PLUS
const baseUrl = 'https://steelwmstest.cndmaterial.com/framework';
// #endif
const esbBaseUrl = 'https://esbrun.chinacnd.com:20001/esb'
const esbBaseCode = 'S04';
const esbBaseAppKey = '4c83c150-0852-11e5-be68-005056a68c0b'

const projectKey = 'wms'
//封装获得uuid
function getUUID() {
	return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, c => {
		return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
	})
}
function version_need_update(curV, reqV) {
	if (curV && reqV) {
		return curV !== reqV
	//   //将两个版本号拆成数字
	// 	let arr1 = curV.split('.'), arr2 = reqV.split('.')
	// 	let minLength = Math.min(arr1.length, arr2.length),
	// 	position = 0,
	// 	diff = 0;
	// 	while (position < minLength && ((diff = parseInt(arr1[position]) - parseInt(arr2[position])) == 0)) {
	//     position++;
	// }
	// diff = (diff != 0) ? diff : (arr1.length - arr2.length)
	// 	//若curV小于reqV，则返回true
	// 	return diff < 0;
	} else {
	    //输入为空
	    console.log("版本号不能为空")
	    return false
	}
}
//封装时间
function getReqTime() {
	let date = new Date();
	return date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) +
		pad2(date.getMinutes()) + pad2(date.getSeconds()) + date.getMilliseconds();
}
function pad2(n) {
	return n < 10 ? '0' + n : n
}
function uniToRpx(px) {
	return 750 * px / uni.getSystemInfoSync().windowWidth
}
function add0(m) {return m < 10 ? '0' + m : m }
function formatTime(timestamp) {
	//shijianchuo是整数，否则要parseInt转换
	let time = new Date(timestamp);
	let y = time.getFullYear();
	let m = time.getMonth()+1;
	let d = time.getDate();
	let h = time.getHours();
	let mm = time.getMinutes();
	let s = time.getSeconds();
	// return y + '-' + add0(m) + '-' + add0(d) + '' + add0(h) + ':' + add0(mm) + ':' + add0(s)
	// return `${y}-${add0(m)}-${add0(d)} ${add0(h)}:${add0(mm)}:${add0(s)}`
	return `${y}-${add0(m)}-${add0(d)}`
}
export {
	baseUrl,
	esbBaseUrl,
	esbBaseCode,
	esbBaseAppKey,
	projectKey,
	getUUID,
	version_need_update,
	uniToRpx,
	formatTime,
	getReqTime
}

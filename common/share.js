export default {
	data() {
		return {
			share: {
				title: '预约送货',
				imageUrl: '../static/images/my/avatar.png',	//图片
			}
		}
	},
	onShareAppMessage(res) { //发送给朋友
		return {
			title: this.share.title,
			imageUrl: this.share.imageUrl,
			path: "/pages/index/index"	//分享默认打开是小程序首页
		}
	},
	onShareTimeline(res) { //分享到朋友圈
		return {
			title: this.share.title,
			imageUrl: this.share.imageUrl,
			path: "/pages/index/index"	//分享默认打开是小程序首页
		}
	},
}

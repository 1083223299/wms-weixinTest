## navbar 导航栏

### 说明
自定义导航栏


### 引入

```javascript
<script>
    import cndNavbar from '@/components/cnd-navbar.vue'
        
    components:{
        cndNavbar
    },
</script>
```

### 代码演示
<!-- DEMO -->
	```html
		<cnd-navbar color="white" :backLevel="backLevel" isBack>
			<block slot="content">审批</block>
			<block slot="right">
				<view @click="filterObj.visible = true" style="display: flex; align-items: center;">
					<image @click="filterObj.visible = true" src="../../static/images/common/filter.png" style="width: 32rpx; height: 32rpx; margin-right: 10rpx;"></image>
					<text @click="filterObj.visible = true"   style="color: #408CDA;">筛选</text>
				</view>
			</block>
		</cnd-navbar>
	```

####  Props

| 属性 | 说明 | 类型 | 默认值 | 备注 |
| ---- | ----- | ------ | ------ | ------ |
| color | 风格 | String | `white` |
| backName | 左边文字 | String | `返回` | - |
| isWebBack | 是否是外联的返回操作 |Boolean|`false`| - |
| isBack |是否可返回 | Boolean | `true` | - |
| backLevel | 返回级数 | Number | `1` | - |


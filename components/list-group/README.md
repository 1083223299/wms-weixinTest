## Listcard 列表卡片

用于展示列表详情。

### 引入

```javascript
<script>
    import listTitle from "../listcard/list-title.vue"
    import listContent from "../listcard/list-content.vue"
    import listFooter from "../listcard/list-footer.vue"
    import listGroup from "../listcard/list-group.vue"    
    
    components:{
        listTitle,
        listContent,
        listFooter,
        listGroup
    },
</script>
```

### 代码演示
<!-- DEMO -->
#### 列表头部
展示所有列表卡片头部样式，包括是否展示icon和status状态。

#### 列表内容
展示所有列表卡片内容样式，包括列表内容省略、左右对齐样式、多行展示样式等。

#### 列表底部
展示所有列表卡片底部样式，包括是否有文字、是否有按钮或共存情况。

#### 基础样式
使用listTitle、listContent、listFooter进行组合，组成最基本样式。

#### 多样化
使用listTitle、listContent、listFooter进行自由组合，丰富展示。

### API

#### listGroup Props

|属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
|-|-|-|-|用来包裹listcard其他元素的最外层listgroup，不需要任何参数。|

#### listTitle Props

|属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
|icon|列表图标|String|`icon-cnd-list`|若有图标，传入图标名称，如“icon-cnd-today”。|
|title|列表名称|String|-|必须值。用以展示的列表名称。|
|status|列表状态|String|`null`|若需要展示列表状态，直接传入状态字段即可。|
|rightLabel|右标题|String|`null`|是否展示右标题，需要展示传入右标题名称。|
|rightDisabled|右标题是否禁用|Boolean|`false`|右标题是否禁用，无法传出点击事件，并且禁用展示。|
|leftLabel|左插槽|Boolean|`false`|是否开启左插槽。|

#### listContent Props

|属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
|title|内容标题|String|-|必须值，内容标题。|
|value|内容|String|`null`|-|
|type|对齐方式|String|`alignBoth`|`alignBoth`两端对齐；`alignLeft`左右对齐|
|titleColor|标题颜色|String|`default`|`default`黑色；`blue`蓝色；`red`红色；`green`绿色|
|valueColor|内容颜色|String|`default`|`default`黑色；`blue`蓝色；`red`红色；`green`绿色|

#### listFooter Props

|属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
|value|内容|String|`null`|卡片底部文字。|
|button|按钮|Array|`null`|卡片底部按钮。传入按钮所需参数所形成的数组，按钮参数详情请见button。按钮尺寸默认为small，不可调整。|

#### 其他
##### list-title事件 @action()
右标题点击事件。
##### list-title事件 @leftAciton()
左插槽点击事件。
##### list-footer事件 @clickEvent(e)
当footer有传入按钮时，利用clickEvent事件将按钮点击事件传出，其中e为按钮传入的顺序。


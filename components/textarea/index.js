//  CndTextarea 是对应组件的名字
import CndTextarea from './cnd-textarea.vue';
CndTextarea.install = Vue => Vue.component(CndTextarea.name, CndTextarea);
export default CndTextarea;
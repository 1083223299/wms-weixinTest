## Textarea 多行输入框

### 说明

表单元素

### 引入

```javascript
<script>
    import cndTextarea from '../textarea/cnd-textarea.vue'
        
    components:{
        cndTextarea
    },
</script>
```

### 代码演示
<!-- DEMO -->

#### 禁用状态
展示禁用状态。

#### 基础样式

### API

#### Textarea Props

|属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
|v-model|绑定值|String|``||
|type|textarea风格 |String|`outlined`|outlined: 带边框，可独立使用，flat: 不带边框，通常配合单元格使用|
|name|原生属性|String|``||
|disabled|是否禁用|Boolean|`false`||
|placeholder|原生属性，占位符|String|-||

#### 其他

自行补充。

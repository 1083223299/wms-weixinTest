// CndDatepicker
import CndDatepicker from './cnd-datepicker.vue';
CndDatepicker.install = Vue => Vue.component(CndDatepicker.name, CndDatepicker);
export default CndDatepicker;